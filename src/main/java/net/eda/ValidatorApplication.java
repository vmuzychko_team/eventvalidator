package net.eda;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class ValidatorApplication
{
    public static final String ALL_EVENTS = "";
    public static final String FANOUT = "fanout";
    @Value("${host.name:localhost}")
    public String hostName;
    @Value("${new.events.exchange:new.events}")
    private String newEventsExchange;
    @Value("${validation.exchange:validated.events}")
    private String validatedEventsExchange;
    @Value("${validation.queue.name:validate}")
    private String validatingQueue;

    @Bean
    Connection connection()
    {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(hostName);
        try
        {
            return connectionFactory.newConnection();
        }
        catch (IOException | TimeoutException e)
        {
            throw new BeanCreationException("Unable to establish connection.", e);
        }
    }

    @Bean
    Channel channel(Connection connection)
    {
        try
        {
            Channel channel = connection.createChannel();

            channel.queueDeclare(validatingQueue, true, false, false, null);
            channel.queueBind(validatingQueue, newEventsExchange, ALL_EVENTS);

            channel.exchangeDeclare(validatedEventsExchange, FANOUT);
            return channel;
        }
        catch (IOException e)
        {
            throw new BeanCreationException("Unable to create a channel.", e);
        }
    }

    public static void main(String[] args)
    {
        SpringApplication.run(ValidatorApplication.class, args);
    }
}
