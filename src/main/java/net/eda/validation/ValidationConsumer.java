package net.eda.validation;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import net.eda.exception.EdaException;
import net.eda.models.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static net.eda.ValidatorApplication.ALL_EVENTS;

@Service
public class ValidationConsumer extends DefaultConsumer
{
    @Autowired
    private List<Validator> validators;
    @Autowired
    private Connection connection;
    @Autowired
    private Channel channel;
    @Value("${validation.exchange:validated.events}")
    private String validatedEventsExchange;

    public ValidationConsumer(Channel channel, @Value("${validation.queue.name:validate}") String validatingQueue)
    {
        super(channel);
        try
        {
            getChannel().basicConsume(validatingQueue, false, this);
        }
        catch (IOException e)
        {
            throw new EdaException("Unable to instantiate validation consumer", e);
        }
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
        throws IOException
    {
        getChannel().basicAck(envelope.getDeliveryTag(), false);
        Event event = new Event(body);
        validators.forEach(validator -> validator.validate(event));
        getChannel().basicPublish(validatedEventsExchange, ALL_EVENTS, null, event.toByteArray());
        System.out.println("=== debug ======= validated =========== " + event);
    }

    @PreDestroy
    public void preDestroy() {
        try
        {
            getChannel().close();
            this.connection.close();
        }
        catch (IOException | TimeoutException e)
        {
            throw new EdaException("Unable to close connection.", e);
        }
    }
}
