package net.eda.validation;

import net.eda.models.Event;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(value = 2)
public class PayloadValidator implements Validator
{
    @Override
    public void validate(Event event)
    {
        // todo: fix dummy
        event.addValidationError("payload error");
    }
}
