package net.eda.validation;

import net.eda.models.Event;


public interface Validator
{
    void validate(Event event);
}
