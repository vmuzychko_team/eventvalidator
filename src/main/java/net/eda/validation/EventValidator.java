package net.eda.validation;

import net.eda.models.Event;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;



@Service
@Order(value = 1)
public class EventValidator implements Validator
{
    @Override
    public void validate(Event event)
    {
        // todo: fix dummy
        event.addValidationError("event error");
    }
}
